<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php 
  print render($title_prefix); 
  if (!$page) { ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php
  }
  
  print render($title_suffix);

  if ($display_submitted) {?>
      <div class="meta submitted">
          <?php 
          print $user_picture;
          print $submitted; ?>
      </div>
  <?php
  }
  ?>
  <div class="image clearfix">
      <?php
      print render($content['field_iconimage']);
      ?>
  </div>

  <div class="meta clearfix">
      <h4>Posted on 
          <?php print format_date($node->created, 'article');?>
          by 
          <?php
          $term = array_shift(taxonomy_term_load_multiple($node->taxonomy_vocabulary_3['und']));
          $localOrNetwork =  $term->name;
          echo $localOrNetwork;
          ?>
      </h4>
  </div>

  <div class="content clearfix" <?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_iconimage']);
      print render($content);
    ?>
  </div>

  <?php
  // Remove the "Add new comment" link on the teaser page or if the comment
  // form is being displayed on the same page.
  if ($teaser || !empty($content['comments']['comment_form'])) {
    unset($content['links']['comment']['#links']['comment-add']);
  }
  // Only display the wrapper div if there are links.
  $links = render($content['links']);
  if ($links) {
  ?>
      <div class="link-wrapper">
          <?php print $links; ?>
      </div>
  <?php 
  }
  print render($content['comments']); 
  ?>
  <div>
      <?php
      print theme('file_formatter_table',array('items'=>$upload['und']));
      ?>
  </div>
  <div>
      <?php
      $terms = array_merge(
        taxonomy_term_load_multiple($node->taxonomy_vocabulary_6['und']),
        taxonomy_term_load_multiple($node->taxonomy_vocabulary_4['und']) );
      $term_names = array();
      foreach ($terms as $term) {
          $term_names[] = $term->name;
      }
      ?>
      <h4>TAGS: <?=implode(', ', $term_names);?></h4>
  </div>
</div>
<div class='row node-related-content'>
  <div class='col-md-6'>
      <h2>More from <?=$localOrNetwork?></h2>
      <?php
      //can't get this to work
      //$view = views_get_view('Local_or_network_posts');
      //$view->set_arguments(array($localOrNetwork));
      //$view->set_arguments(array($node->nid));
      //$view->set_display('panel_pane_1');
      $view = views_get_view('Latest_nodes');
      $view->set_display('default');
      $view->execute();
      print $view->render();
      ?>
  </div>
  <div class='col-md-6'>
      <h2>Similar posts</h2>
      <?php
      $view = views_get_view('Latest_nodes');
      $view->set_display('default');
      $view->execute();
      print $view->render();
      ?>
  </div>
</div>