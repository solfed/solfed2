<header id="navbar" role="banner" class="navbar navbar-fixed-top">
  <div class="container">
    <div id="header-content">  
      <div class="navbar-header">
        
        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobile-nav">
            <span class="glyphicon glyphicon-align-justify"></span>
            <span class="sr-only">Toggle primary navigation</span>
          </button>
        <a class="logo navbar-btn" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="/<?php print  path_to_theme(); ?>/images/masthead_title_short.png" alt="<?php print t('Home'); ?>" />
        </a>
      </div>

      <?php 
      if (!empty($primary_nav)) {
      ?>
        <nav id="primary-nav" class="pull-right hidden-xs" role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
        </nav>
      <?php
      }
      $mobile_menu_links = menu_navigation_links('menu-mobile');
      if (!empty($mobile_menu_links)) {
        //navbar-collapse collapse 
        ?>
        <nav id="mobile-nav" class="collapse" role="navigation">
          <?php
          $mobile_menu = theme('links', array('links' => menu_navigation_links('menu-mobile'), 'attributes' => array()));
          print($mobile_menu);
          ?>
        </nav>
      <?php
      }
      ?>
    </div>
  </div>
</header>
<?php if (!empty($secondary_nav) || !empty($page['navigation'])): ?>
  <div class="navbar-inverse navbar-fixed-bottom"> 
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#secondary-nav">
        <span class="glyphicon glyphicon-align-justify"></span>
        <span class="sr-only">Toggle secondary navigation</span>
      </button>
      <a class="navbar-brand" href="#">User Menu</a>
    </div>
      <nav id="secondary-nav" class="navbar-collapse collapse" role="navigation">
        <div class="container">
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </div>
      </nav>
  </div>
<?php endif; ?>

<div class="container"><div class='row' id="top-spacer">&nbsp;</div></div>
<div class="main-container container">

  <header role="banner" id="page-header">
    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->

  <div class="row">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php /* if (!empty($breadcrumb)): print $breadcrumb; endif; */ ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>
  </div>
</div>
<div class="container"><div class='row' id="bottom-spacer">&nbsp;</div></div>
<footer class="footer">
  <div class="container">
    <div class="row">
      <div id="footer-content" class="col-md-12">
        <?php print render($page['footer']); ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <h4>Solidarity Federation</h4>
        British & Irish section of <a href='http://www.iwa-ait.org/' target='_blank'>International Workers Association</a>
      </div>
      <div class="col-md-2">
        <?php
        $links = theme('links', array('links' => menu_navigation_links('menu-footer'), 'attributes' => array('class'=> array('footer-menu')) ));
        print( $links);
        ?>
      </div>
      <div class="col-md-2">
        <ul>
          <li><a href='/user/login'>Login</a></li>
        </ul>
      </div>
    </div>
    <div class="row last">
      <div class="col-md-8">
        <a href='http://www.iwa-ait.org/' target='_blank'><img src="/<?= path_to_theme(); ?>/images/iwafooter_small.png" /></a>
      </div>
      <div class="col-md-2 footer-social-links">
        <a href='https://twitter.com/solfed_iwa' target='_blank'><span class='sf-icon sf-white-twitter'></span></a>
        <a href='https://www.facebook.com/solfed' target='_blank'><span class='sf-icon sf-white-facebook'></span></a>
      </div>
    </div>
  </div>
</footer>
