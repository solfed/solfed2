<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php 
  print render($title_prefix); 
  if (!$page) { ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php
  }
  
  print render($title_suffix);

  if ($display_submitted) {?>
      <div class="meta submitted">
          <?php 
          print $user_picture;
          print $submitted; ?>
      </div>
  <?php
  }
  ?>
  <div class="image clearfix">
  <?php
  print render($content['field_iconimage']);
  ?>
  </div>

  <div class="meta clearfix">
      <h4>Posted on 
      <?php
        print format_date($node->created, 'article');
      ?>
      by 
      <?php
      $term = array_shift(taxonomy_term_load_multiple($node->taxonomy_vocabulary_3['und']));
      print $term->name;
      ?></h4>
  </div>

  <div class="content clearfix" <?php print $content_attributes; ?>>
  <?php
  // We hide the comments and links now so that we can render them later.
  hide($content['comments']);
  hide($content['links']);
  hide($content['field_iconimage']);
  print render($content);
  ?>
  </div>

  <?php
  // Remove the "Add new comment" link on the teaser page or if the comment
  // form is being displayed on the same page.
  if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
  }
  // Only display the wrapper div if there are links.
  $links = render($content['links']);
  if ($links) {
  ?>
      <div class="link-wrapper">
          <?php print $links; ?>
      </div>
  <?php
  }
  ?>

</div>
