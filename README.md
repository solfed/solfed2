SolFed theme using Bootstrap/Sass
---------------------------------

I've refactored the theme to compile the bootstrap css itself from sass, for two reasons:

* Easy to customise colours/fonts etc (see sass/\_variables.scss)
* We can use bootstrap features like the grid, list formats, etc in css using sass mixins instead of adding classes to the html

I've put an example of how this works at the bottom of sass/\style.scss. The contact form is put into a grid using css instead of having to write a Drupal theme override to add the column classes to the form.

I also removed all the legacy files and moved from jekyll to compass, since we were only using it to compile the sass. To use:

    gem install compass bootstrap-sass
		compass watch

And correct the link to point to solfed3 again.
